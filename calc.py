import sys
import operator


if len(sys.argv) == 4:
   try:
       funcion= sys.argv[1]
       operando1= float(sys.argv[2])
       operando2= float(sys.argv[3])

       dic= {"multiplicacion": operator.mul,
               "division": operator.truediv,
               "sumar": operator.add,
               "restar": operator.sub}
       print(dic[funcion])
       result = dic[funcion](operando1, operando2)
       print("El resultado de", funcion, "los numeros: ", operando1, " y ", operando2, "es igual a = ", result)
   except ValueError:
       sys.exit("Incorrect format")
   except ZeroDivisionError as err:
       print('Handling run-time error:', err)
   except:
       print("Unexpected error:", sys.exc_info()[0])
else:
   sys.exit("<funcion(sumar, restar, multiplicar o dividir)><operando1><operando2>")
