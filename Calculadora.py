#!/usr/bin/python3

import sys
import operator
 

if len(sys.argv) == 5:
	try:
		funcion = sys.argv[1]
		operando1 = float(sys.argv[2])
		operando2 = float(sys.argv[3])
	except ValueError:
		sys.exit("Incorrect format")

	dict= {"multiplicacion" : operator.mul,
		"division" : operator.truediv,
		"suma": operator.add,
		"resta" : operator.sub}
		

	ans = dict[funcion](operando1,operando2)

	print("Result = {}".format(ans))
else:
	sys.exit("<funcion> <operando1> <operando2>")
